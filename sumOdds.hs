sumOdds :: [Int] -> Int
sumOdds []     = 0
sumOdds (x:xs) = if mod x 2 == 0 then sumOdds xs else x + sumOdds xs

testSumOdds = sumOdds [7,4,10,2,5] 